.DEFAULT_GOAL := fsf
install: composer.json
	$(shell composer install)
update: composer.lock
	$(shell composer update -v)
clean: composer.lock vendor/
	$(shell rm -rf composer.lock ./vendor 2> /dev/null)
check-style: src/
	@php vendor/bin/phpcs --standard=./ruleset.xml src/
fsf:
	@echo "software should be free\n \e[1mhttps://www.gnu.org/philosophy/shouldbefree.html\e[0m"
